%%{init: {'theme': 'base', 'themeVariables': { 'lineColor': '#404040'}}}%%
        graph LR
          Input1[[fa:fa-file-archive-o .BAM]] --> Node1{{ 1. <br/> BlastQC }}
          Input2[[fa:fa-file-archive-o .FASTQ]] --> Node2{{ 2. <br/> Minimap2 <br/> Align }}
          Node1 -.-> Node2
          Node2 -.-> Node3{{ 3. <br/> PycoQC }}
          Node3 -.-> Node4{{ 4. <br/> Picard Merge <br/> SAM Files }}
          Node2 -.-> Node4
          Node4 -.-> Node5{{ 5. <br/> SVIM }}
          Output([fa:fa-sticky-note-o Report ])
          Node5 ---> Output
          subgraph readSetSteps[   ]
            Node1
            Node2
            Node3
          end
          subgraph sampleSetSteps[  ]
            Node5
            Output
          end
          classDef greenRect fill:#E4FBF4
          class readSetSteps greenRect
          classDef blueRect fill:#DEF6FC
          class sampleSetSteps blueRect
          classDef nodefill fill:#ffffff,stroke:black,stroke-width:2px
          classDef nodefillblack fill:black,stroke:black,color:white,stroke-width:2px
          class Node1 nodefill
          class Node2 nodefill
          class Node3 nodefill
          class Node5 nodefill
          class Node4 nodefillblack
